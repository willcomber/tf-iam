locals {
  groups_csv = csvdecode(file("${path.module}/groups.csv"))
  groups = { for r in local.groups_csv : "${r.dg}-${r.name}" => r }
  environments = yamldecode(file("${path.module}/environments.yaml"))
  policies = yamldecode(file("${path.module}/policies.yaml"))
}

resource "dynatrace_iam_group" "prod_user_group" {
  for_each = local.groups
  name        = "${each.value.dg} - ${each.value.product} - user - prod"
  description = "Production User Group for ${each.value.product} (${each.value.dg})"
}

resource "dynatrace_iam_group" "prod_power_user_group" {
  for_each = local.groups
  name        = "${each.value.dg} - ${each.value.product} - power user - prod"
  description = "Production Power User Group for ${each.value.product} (${each.value.dg})"
}


resource "dynatrace_iam_group" "preprod_user_group" {
  for_each = local.groups
  name        = "${each.value.dg} - ${each.value.product} - user - preprod"
  description = "PreProduction User Group for ${each.value.product} (${each.value.dg})"
}

resource "dynatrace_iam_group" "preprod_power_user_group" {
  for_each = local.groups
  name        = "${each.value.dg} - ${each.value.product} - power user - preprod"
  description = "PreProduction Power User Group for ${each.value.product} (${each.value.dg})"
}

resource "dynatrace_iam_group" "nonprod_user_group" {
  for_each = local.groups
  name        ="${each.value.dg} - ${each.value.product} - user - nonprod"
  description = "NonProduction User Group for ${each.value.product} (${each.value.dg})"
}

resource "dynatrace_iam_group" "nonprod_power_user_group" {
  for_each = local.groups
  name        = "${each.value.dg} - ${each.value.product} - power user - nonprod"
  description = "NonProduction Power User Group for ${each.value.product} (${each.value.dg})"
}

resource "dynatrace_iam_policy_bindings_v2" "prod_user_bind" {
  for_each = local.groups  
  group = dynatrace_iam_group.prod_user_group[each.key].id
  environment   = local.environments.production
  policy {
    id = local.policies.serviceuser
    parameters = {
      "managementZone" : "${each.value.dg}-${each.value.product}"
    }
  }  
  policy {
    id = local.policies.defaultbucket
    parameters = {
      "dg" : each.value.dg
      "product" : each.value.product
    }
  }
  policy {
    id = local.policies.teambucket
    parameters = {
      "bizBucket" :  "${each.value.dg}-${each.value.product}-bizevents"
      "logsBucket" :  "${each.value.dg}-${each.value.product}-logs"
    }
  } 
}

resource "dynatrace_iam_policy_bindings_v2" "prod_power_user_bind" {
  for_each = local.groups  
  group = dynatrace_iam_group.prod_power_user_group[each.key].id
  environment   = local.environments.production
  policy {
    id = local.policies.servicepu
    parameters = {
      "managementZone" :  "${each.value.dg}-${each.value.product}"
    }
  }  
  policy {
    id = local.policies.defaultbucket
    parameters = {
      "dg" : each.value.dg
      "product" : each.value.product
    }
  }
  policy {
    id = local.policies.teambucket
    parameters = {
      "bizBucket" :  "${each.value.dg}-${each.value.product}-bizevents"
      "logsBucket" :  "${each.value.dg}-${each.value.product}-logs"
    }
  }  
}

resource "dynatrace_iam_policy_bindings_v2" "preprod_user_bind" {
  for_each = local.groups  
  group = dynatrace_iam_group.preprod_user_group[each.key].id
  environment   = local.environments.preproduction
  policy {
    id = local.policies.serviceuser
    parameters = {
      "managementZone" :  "${each.value.dg}-${each.value.product}"
    }
  }  
  policy {
    id = local.policies.defaultbucket
    parameters = {
      "dg" : each.value.dg
      "product" : each.value.product
    }
  }
  policy {
    id = local.policies.teambucket
    parameters = {
      "bizBucket" :  "${each.value.dg}-${each.value.product}-bizevents"
      "logsBucket" :  "${each.value.dg}-${each.value.product}-logs"
    }
  }  
}

resource "dynatrace_iam_policy_bindings_v2" "preprod_power_user_bind" {
  for_each = local.groups  
  group = dynatrace_iam_group.preprod_power_user_group[each.key].id
  environment   = local.environments.preproduction
  policy {
    id = local.policies.servicepu
    parameters = {
      "managementZone" :  "${each.value.dg}-${each.value.product}"
    }
  }  
  policy {
    id = local.policies.defaultbucket
    parameters = {
      "dg" : each.value.dg
      "product" : each.value.product
    }
  }
  policy {
    id = local.policies.teambucket
    parameters = {
      "bizBucket" :  "${each.value.dg}-${each.value.product}-bizevents"
      "logsBucket" :  "${each.value.dg}-${each.value.product}-logs"
    }
  }   
}

resource "dynatrace_iam_policy_bindings_v2" "nonprod_user_bind" {
  for_each = local.groups  
  group = dynatrace_iam_group.nonprod_user_group[each.key].id
  environment   = local.environments.nonproduction
  policy {
    id = local.policies.serviceuser
    parameters = {
      "managementZone" :  "${each.value.dg}-${each.value.product}"
    }
  }  
  policy {
    id = local.policies.defaultbucket
    parameters = {
      "dg" : each.value.dg
      "product" : each.value.product
    }
  }
  policy {
    id = local.policies.teambucket
    parameters = {
      "bizBucket" :  "${each.value.dg}-${each.value.product}-bizevents"
      "logsBucket" :  "${each.value.dg}-${each.value.product}-logs"
    }
  }   
}

resource "dynatrace_iam_policy_bindings_v2" "nonprod_power_user_bind" {
  for_each = local.groups  
  group = dynatrace_iam_group.nonprod_power_user_group[each.key].id
  environment   = local.environments.nonproduction
  policy {
    id = local.policies.servicepu
    parameters = {
      "managementZone" :  "${each.value.dg}-${each.value.product}"
    }
  }  
  policy {
    id = local.policies.defaultbucket
    parameters = {
      "dg" : each.value.dg
      "product" : each.value.product
    }
  }
  policy {
    id = local.policies.teambucket
    parameters = {
      "bizBucket" :  "${each.value.dg}-${each.value.product}-bizevents"
      "logsBucket" :  "${each.value.dg}-${each.value.product}-logs"
    }
  } 
}