locals {
  users_data = jsondecode(file("${path.module}/users.json"))
}

resource "dynatrace_iam_user" "users" {
  for_each = {
    for idx, user in local.users_data : idx => {
      email             = user.email
      poweruser         = user.poweruser
      production_access = user.production_access
      groups = [for group in user.groups : {
        dg      = group.dg
        service = group.service
      }]
    }
  }

  email = each.value.email

  groups = compact(flatten([
    for group in each.value.groups : (
      each.value.poweruser ? [
        each.value.production_access ? dynatrace_iam_group.prod_power_user_group["${group.dg}-${group.service}"].id : null,
        dynatrace_iam_group.preprod_power_user_group["${group.dg}-${group.service}"].id,
        dynatrace_iam_group.nonprod_power_user_group["${group.dg}-${group.service}"].id
        ] : [
        each.value.production_access ? dynatrace_iam_group.prod_user_group["${group.dg}-${group.service}"].id : null,
        dynatrace_iam_group.preprod_user_group["${group.dg}-${group.service}"].id,
        dynatrace_iam_group.nonprod_user_group["${group.dg}-${group.service}"].id
      ]
    )
  ]))
}
