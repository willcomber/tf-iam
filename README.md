# TF IAM

This repo is to manage user access as code


## Adding users

Edit the users.csv file to add the new user required.

E.g.


|dg|service|email|prod|poweruser|
|---|---|---|---|---|
|borders|cds|test@hmrc.gov.uk|true|true|


## Onboarding a new service - creating a new group and required config

Update the groups.csv file

|dg|name|
|---|---|
|case|test|